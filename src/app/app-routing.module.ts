import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddComponent } from './Components/add/add.component';
import { DisplayComponent } from './Components/display/display.component';

const routes: Routes = [
  {path: '',redirectTo:'/add',pathMatch:'full'},
  {path:'add', component:AddComponent},
  {path:'display',component:DisplayComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
