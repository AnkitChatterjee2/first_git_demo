import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddComponent } from './Components/add/add.component';
import { SchoolComponent } from './school/school.component';
import { DisplayComponent } from './Components/display/display.component';
import { StudentDetailsComponent } from './details/student-details/student-details.component';
import { SumComponent } from './Components/sum/sum.component';
import { SelectComponent } from './select/select.component';
import { StudentsDetailsComponent } from './students-details/students-details.component';
import { StudentAddressComponent } from './student-address/student-address.component';

@NgModule({
  declarations: [
    AppComponent,
    AddComponent,
    SchoolComponent,
    DisplayComponent,
    StudentDetailsComponent,
    SumComponent,
    SelectComponent,
    StudentsDetailsComponent,
    StudentAddressComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
